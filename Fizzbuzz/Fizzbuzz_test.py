import unittest
from Fizzbuzz import fizzbuzzpop

class testfunc(unittest.TestCase):

    def setUp(self):
        print("setting up")

    def tearDown(setUp):
        print()

    def test_3_is_fizz(self):
        self.assertEqual(fizzbuzzpop(3), 'Fizz')

    def test_5_is_buzz(self):
        self.assertEqual(fizzbuzzpop(5), 'Buzz')

    def test_15_is_fizzbuzz(self):
        self.assertEqual(fizzbuzzpop(15), 'FizzBuzz')

    def test_1_is_1(self):
        self.assertEqual(fizzbuzzpop(1), 1)

    def test_2_is_2(self):
        self.assertEqual(fizzbuzzpop(2), 2)

    def test_6_is_fizz(self):
        self.assertEqual(fizzbuzzpop(6), 'Fizz')

    def test_10_is_buzz(self):
        self.assertEqual(fizzbuzzpop(10), 'Buzz')

    def test_30_is_fizzbuzz(self):
        self.assertEqual(fizzbuzzpop(30), 'FizzBuzz')


if __name__ == '__main__':
    unittest.main()