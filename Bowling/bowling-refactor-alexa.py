import random


class Bowling():

    def __init__(self):
        self.score_card = {}
        print('\nWelcome to Bowlarama!\n')

    def take_a_turn(self, frame):
        score = []
        first_roll = random.randint(0, 10)
        score.append(first_roll)
        second_roll = random.randint(0, 10 - first_roll)
        score.append(second_roll)
        print('You rolled a', score)
        self.display_spare_strike_message(frame, score)
        return score

    def display_spare_strike_message(self, frame, score):
        if frame != 10:
            if score == [10, 0]:
                print('STRIKE! Expect double points on your next go!')
            elif sum(score) == 10:
                print('SPARE! Expect double points on the first roll of your next go!')
        else:
            if score == [10, 0]:
                print('STRIKE! You can now roll again!')
            elif sum(score) == 10:
                print('SPARE! You can now roll one more ball!')

    def update_score(self, frame, score):
        self.score_card[frame] = [score]
        if frame == 1:
            self.score_card[1].append(score)
        else:
            first_roll = self.score_card[frame - 1][0][0]
            both_rolls = (self.score_card[frame - 1][0][0] + self.score_card[frame-1][0][1])
            score_copy = score.copy()

            # strike
            if first_roll % 10 == 0 and first_roll != 0:
                score_copy = [2 * x for x in score_copy]
                self.score_card[frame].append(score_copy)
            # spare
            elif both_rolls % 10 == 0 and both_rolls != 0:
                score_copy[0] = score_copy[0]*2
                self.score_card[frame].append(score_copy)

            # neither
            else:
                self.score_card[frame].append(score_copy)

        # strike on 10th frame
        if frame == 10 and score[0] % 10 == 0 and score[0] != 0:
            self.strike_in_10th_frame()

        # spare on 10th frame
        elif frame == 10 and sum(score) % 10 == 0 and sum(score) != 0:
            self.spare_in_10th_frame()

        return self.score_card

    def spare_in_10th_frame(self):
        extra_roll = random.randint(0, 10)
        self.score_card[frame][1].append(extra_roll)
        print('You rolled', extra_roll, '\n')

    def strike_in_10th_frame(self):
        first_extra_roll = random.randint(0, 10)
        self.score_card[frame][1].append(first_extra_roll)
        print('You rolled', first_extra_roll, '\n')
        if first_extra_roll == 10:
            print('STRIKE! You got ANOTHER strike! You get to roll one more time!')
            second_extra_roll = random.randint(0, 10)
            print('You rolled', second_extra_roll, '\n')
            self.score_card[frame][1].append(second_extra_roll)

    def total_score(self, score_card):
        total = 0
        for key, value in score_card.items():
            total += sum(value[1])
        return total

    # def print_score_card


if __name__ == '__main__':

    game = Bowling()

    for frame in range(1, 11):
        print('Frame', frame)
        game.update_score(frame, game.take_a_turn(frame))

    print('Congratulations! Your final score is:', game.total_score(game.score_card))